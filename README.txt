INTRODUCTION
------------
  Bcc Logs all outgoing mail from your site.Send all outgoing mail to Bcc email id that passes through drupal_mail function

Features
--------
  1. Bcc Logging all outgoing mail
  2. Mail verification for bcc email id.
  3. Activation/deactivation mail alert.
  4. Editable mail template for verification, Activation and deactivation mail

INSTALLATION
------------
  1. Copy bcc_mail_logger folder to modules directory.
  2. At admin/build/modules enable the "Bcc Mail Logger" module.
  4. Configure the module at admin/settings/bccmaillogger.

Issue with Phpmailer module 
--------------------------
  if you want to use this module with Phpmailer module then you need to apply following patch for it.
  Phpmailer module Bug: The Cc and Bcc destination address fields are not the correct case . . .
  please see http://drupal.org/node/927646
CREDITS
-------
  This project has been sponsored by: Stigasoft(http://www.stigasoft.com)
