<?php
/**
 * @file
 * bcc_mail_logger.admin.inc used to handle Menu callback .
 * 
 */

/**
 * Form builder for bcc_mail_logger settings page.
 *
 * @ingroup forms
 * @see bcc_mail_logger_settings_form_validate()
 * @see bcc_mail_logger_settings_form_submit()
 */
function bcc_mail_logger_settings_form($form_state = array()) {

  $email_token_help = t('Available variables are:') .' !username, !site, !bccemailid';
  if (empty($form_state['post'])) {
    if (variable_get('bccmaillogger_activeemailid', '') && variable_get('bccmaillogger_on', 0)) {
      drupal_set_message(t('Currently active Bcc email : %email.', array('%email' => variable_get('bccmaillogger_activeemailid', ''))));
    }
    if (variable_get('bccmaillogger_activeemailid', '') != variable_get('bccmaillogger_emailid', '') && variable_get('bccmaillogger_acknowledgement_emailid', '')) {
      drupal_set_message(t('Bcc logging activation email sent to %email containing email verification link. System is waiting for email verification from %email. To resend email verification link to %email !clickhere.', array('%email' => variable_get('bccmaillogger_acknowledgement_emailid', ''), '!clickhere' => l(t('click here'), 'admin/settings/bccmaillogger/resend_acknowledgement'))));
    }
  }
  $form['bccmaillogger_on'] = array(
     '#type' => 'checkbox',
     '#title' => t('Activate / Deactivate Bcc Mail Logging'),
     '#default_value' => variable_get('bccmaillogger_on', 0),
     '#description' => t('When set to active, all emails will go to following Bcc email'),
  );
  $form['bccmaillogger_emailid'] = array(
     '#type' => 'textfield',
     '#title' => t('Mail Logger Bcc email'),
     '#default_value' => variable_get('bccmaillogger_emailid', ''),
     '#description' => t('Enter emailid where you want to keep log of all the outgoing emails.'),
  );
  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email templates'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Enable and customize email messages sent to users.'),
  );
  $form['email']['acknowledgement_mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Verification email notification'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Enable and customize email message sent to Bcc logging email for email verification.') .' '. $email_token_help . ', !acknowledgement_url.',
  );
  
  $form['email']['acknowledgement_mail']['bccmaillogger_acknowledgement_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => _bcc_mail_logger_mail_text('bccmaillogger_acknowledgement_mail_subject'),
    '#maxlength' => 180,
  );

  $form['email']['acknowledgement_mail']['bccmaillogger_acknowledgement_mail_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => _bcc_mail_logger_mail_text('bccmaillogger_acknowledgement_mail_body'),
    '#rows' => 7,
  );
  
   $form['email']['activation_mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Activation notification email'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Enable and customize email notification message on activation of Bcc logging.') .' '. $email_token_help . '.',
  );
  $form['email']['activation_mail']['bccmaillogger_activation_mail_notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user when Bcc mail logging gets activated.'),
    '#default_value' => variable_get('bccmaillogger_activation_mail_notify', TRUE),
  );
  $form['email']['activation_mail']['bccmaillogger_activation_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => _bcc_mail_logger_mail_text('bccmaillogger_activation_mail_subject'),
    '#maxlength' => 180,
  );
  $form['email']['activation_mail']['bccmaillogger_activation_mail_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => _bcc_mail_logger_mail_text('bccmaillogger_activation_mail_body'),
    '#rows' => 3,
  );
  $form['email']['deactivation_mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Deactivation notification email'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Enable and customize email notification message on deactivation of Bcc logging.') .' '. $email_token_help . '.',
  );
  $form['email']['deactivation_mail']['bccmaillogger_deactivation_mail_notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user when Bcc mail logging gets deactivated.'),
    '#default_value' => variable_get('bccmaillogger_deactivation_mail_notify', TRUE),
  );
  $form['email']['deactivation_mail']['bccmaillogger_deactivation_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => _bcc_mail_logger_mail_text('bccmaillogger_deactivation_mail_subject'),
    '#maxlength' => 180,
  );
  $form['email']['deactivation_mail']['bccmaillogger_deactivation_mail_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => _bcc_mail_logger_mail_text('bccmaillogger_deactivation_mail_body'),
    '#rows' => 3,
  );
  $form= system_settings_form($form);
  //add custom submit function settings form.
  $form['#submit'][]='bcc_mail_logger_settings_form_submit';
  return $form;
}

/**
 * Form validation function.
 *
 * @see bcc_mail_logger_settings_form()
 */
function bcc_mail_logger_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] != t('Reset to defaults')) {
    if ($form_state['values']['bccmaillogger_emailid'] && !valid_email_address($form_state['values']['bccmaillogger_emailid'])) {
      form_set_error('bccmaillogger_emailid', 'Email Id is invalid.');
    }
    if ($form_state['values']['bccmaillogger_on'] && $form_state['values']['bccmaillogger_emailid']=="") {
      form_set_error('bccmaillogger_emailid', 'Email Id is required.');
    }
  }
  $form_state['values']['lastvalue'] = array(
      'bccmaillogger_on' => variable_get('bccmaillogger_on', 0),
      'bccmaillogger_emailid' => variable_get('bccmaillogger_emailid', '')
    );
}

/**
 * Form Submit function.
 *
 * @see bcc_mail_logger_settings_form()
 */
function bcc_mail_logger_settings_form_submit($form, &$form_state) {
  global $user;
  $params['account'] = $user;
  if ($_POST['op'] != t('Reset to defaults')) {
    if ($form_state['values']['bccmaillogger_on'] && $form_state['values']['bccmaillogger_emailid'] && $form_state['values']['bccmaillogger_emailid'] != variable_get('bccmaillogger_activeemailid', '') && $form_state['values']['bccmaillogger_emailid'] != variable_get('bccmaillogger_acknowledgement_emailid', '')) {
      $notify = _bcc_mail_logger_notify('bccmaillogger_acknowledgement_mail');
      variable_set('bccmaillogger_acknowledgement_emailid', $form_state['values']['bccmaillogger_emailid']);
    }
    
    if ($form_state['values']['bccmaillogger_on'] != $form_state['values']['lastvalue']['bccmaillogger_on'] && variable_get('bccmaillogger_activeemailid', '')) {
      if ($form_state['values']['bccmaillogger_on']) {
        $notify = _bcc_mail_logger_notify('bccmaillogger_activation_mail');
      }
      else {
        $notify = _bcc_mail_logger_notify('bccmaillogger_deactivation_mail');
      }
    }
    watchdog('Bcc Mail Logger', 'Bcc mail logger settings updated by %name.', array('%name' => $user->name), WATCHDOG_NOTICE);
  }
  else {
    if ($form_state['values']['lastvalue']['bccmaillogger_on'] && variable_get('bccmaillogger_activeemailid', '')) {
      $notify = _bcc_mail_logger_notify('bccmaillogger_deactivation_mail');
    }
    variable_del('bccmaillogger_activeemailid');
    variable_del('bccmaillogger_acknowledgement_emailid');
    drupal_set_message(t('All values will be deleted from database. This action can not be undone.'));
    watchdog('Bcc Mail Logger', 'Bcc mail logger settings reseted by %name.', array('%name' => $user->name), WATCHDOG_NOTICE);
  }
}

/**
 * Menu callback; Resend Bcc mail logger acknowledgement notification.
 */
function bcc_mail_logger_acknowledgement_resend() {
  if (variable_get('bccmaillogger_activeemailid', '') != variable_get('bccmaillogger_emailid', '') && variable_get('bccmaillogger_acknowledgement_emailid', '')) {
    $notify = _bcc_mail_logger_notify('bccmaillogger_acknowledgement_mail');
    drupal_set_message(t('acknowledgement notification mailed at %email', array('%email' => variable_get('bccmaillogger_emailid', ''))));
  }
  else {
    drupal_set_message(t('Validation error, please try again.'));
  }
  drupal_goto('admin/settings/bccmaillogger');
}

/**
 * Menu callback; process Bcc mail logger activation link.
 */
function bcc_mail_logger_acknowledgement() {
  $token = check_plain(arg(1));
  $mail = variable_get('bccmaillogger_emailid', '');
  if ($token === md5($mail) && valid_email_address($mail) && $mail != variable_get('bccmaillogger_activeemailid', '')) {
    global $user;
    $params['account'] = $user;
    // if Bcc mail logger active on another emailid.
    if (variable_get('bccmaillogger_activeemailid', '') && $mail != variable_get('bccmaillogger_activeemailid', '')) {
      $notify = _bcc_mail_logger_notify('bccmaillogger_deactivation_mail');
    }
    variable_set('bccmaillogger_activeemailid', $mail);
    $notify = _bcc_mail_logger_notify('bccmaillogger_activation_mail');
    
    $variables = array( '%email' => $mail);
    watchdog('Bcc Mail Logger', 'Bcc mail logger activated at %email', $variables, WATCHDOG_NOTICE);
    drupal_set_message(t('Bcc mail logger activated at %email', $variables));
  }
  else {
    watchdog('Bcc Mail Logger', 'Bcc mail logger verification link has already been used or is no longer valid.', $variables, WATCHDOG_WARNING);
    drupal_set_message(t('You have tried to use Bcc mail logging verification link which has already been used or is no longer valid. Please contact site administrator.'), 'error');
  }
  //Redirect to home page.
  drupal_goto();
}